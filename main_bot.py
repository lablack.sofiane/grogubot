import os
import discord
import random
"""import os
from dotenv import load_dotenv
load_dotenv(dotenv_path="config")"""
from dotenv import load_dotenv
load_dotenv(dotenv_path="config")
from discord.ext import commands



bot = commands.Bot(command_prefix="§")

@bot.event
async def on_ready():
    print("Le bot est prêt !")
    await bot.change_presence(activity=discord.Activity(name="§help ", type=1))


@bot.command(name="del")
async def delete(ctx, number: int):
    """del suivi du nombre de messages à supprimer"""
    messages = await ctx.channel.history(limit=number + 1).flatten()

    for each_message in messages:
        await each_message.delete()

@bot.command(name="grogu")
async def grogu(ctx):
    """pour une image sympatique"""
    await ctx.send(":slight_smile: ", file=discord.File('grogu_bot_name.JPEG'))

@bot.command(name="hello")
async def groguhello(ctx):
    """pour un GIF sympatique"""
    await ctx.send("https://tenor.com/view/grogu-star-wars-mandalorian-baby-yoda-gif-21008340")

@bot.command(name="KEKW")
async def KEKW(ctx):
    await ctx.send("https://tenor.com/view/el-risitas-risitas-wheezingrisitas-gif-21432588")

@bot.command(name="piece")
async def piece(ctx):
    """pour un pile ou face"""
    coin = ["pile", "face"]
    flipchoice = random.choice(coin)
    if str(flipchoice) == "pile":
        await ctx.send("C'est pile !")
    else:
        await ctx.send("C'est face !")

@bot.command(name="dice")
async def dice(ctx):
    """pour jeter un dé à 20 faces"""
    dice = list(range(1,20))
    flipchoice = random.choice(dice)
    await ctx.send(f"C'est {flipchoice} !")

@bot.command(name="ping")
async def ping(ctx):
    """pour une réponse pong"""
    await ctx.send("pong")


bot.run(os.getenv("TOKEN"))

#$del 2