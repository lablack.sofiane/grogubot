#pour accéder aux variables d’environnement on doit import le module os
import os
#import module discord
import discord
from dotenv import load_dotenv
import asyncio
import random

load_dotenv(dotenv_path="config")

#class Intents
default_intents = discord.Intents.default()
default_intents.members = True
client = discord.Client(intents = default_intents)


#on créer une instance de discord.Client
#Client est une classe qui permet de créer un client
#client = discord.Client()

@client.event # c'est un décorateur sert a ce que la fonction soit reconnue comme fonction déclenchée lorsque l'event on_ready est déclenché
#fonction async mot clé pour ajouter une coroutine
async def on_ready():
   #print le bot est prêt dans le terminal s'il est prêt à executer des commandes c a d s 'il s est bien connecté au serveur est prêt à utilisation
   print("Le bot est prêt.")

@client.event
async def on_member_join(member):
   #general_channel: discord.TextChannel = client.get_channel(928070495688069123)
   channel = discord.utils.get(member.guild.channels, name="général")
   #general_zaun_channel: discord.TextChannel = client.get_channel(241244756238270465)
   #await general_channel.send(content=f"Bienvenue sur le serveur {member.display_name} !")
   await channel.send(content=f"Bienvenue sur le serveur {member.display_name} !")


@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')


@client.event
async def on_message(message):
    #on récupère le contenu du message envoyé si c'est ping
    if message.content.lower() == "ping":
       #await coroutine qui peut être mise dans une fonction qui commence par async permet d'éviter que plusieurs choses ne se passent en mm temps
       #récupère le channel dans lequel le message a été envoyé et on y envoie pong
       await message.channel.send("pong")
       #on peut ajouter l’élément delete_after dans notre fonction pour supprimer le message après 5 secondes
       #await message.channel.send("pong",delete_after=5)
    if message.content.startswith("§del"):
       #split sur l'espace [1] récupère le nombre après commande del
       number = int(message.content.split()[1])
       #récupère l'historique des messages dans le salon flatten pour une liste unique
       messages = await message.channel.history(limit=number +1).flatten()
       #boucle sur chaque messages de la liste
       for each_message in messages:
           await each_message.delete()

    if message.content.lower() == "who":
       await message.channel.send('Logged in as '+str(client.user.name) + " user ID : " + str(client.user.id) + " author ID : " + str(message.author.id))

    if message.content.startswith("§piece"):
        coin = ["pile", "face"]
        flipchoice = random.choice(coin)
        if str(flipchoice) == "pile":
          await message.channel.send("C'est pile !")
        else:
          await message.channel.send("C'est face !")

    if message.content.startswith("§dice"):
        dice = list(range(1,20))
        flipchoice = random.choice(dice)
        await message.channel.send(f"C'est {flipchoice} !")
"""
    if message.content.startswith("§dice "+str(int)):
        dice = list(range(1,20))
        flipchoice = random.choice(dice)
        await message.channel.send(f"C'est {flipchoice} !")

        # récupère l'historique des messages dans le salon flatten pour une liste unique
        messages = await message.channel.history(limit=number + 1).flatten()

"""
#on utilise la méthode run avec le token à l'intérieur
client.run(os.getenv("TOKEN"))
