import os
import discord
import random
"""import os
from dotenv import load_dotenv
load_dotenv(dotenv_path="config")"""
from dotenv import load_dotenv
load_dotenv(dotenv_path="config")
from discord.ext import commands
import youtube_dl
import asyncio
#default_intents = discord.Intents.default()
#default_intents.members = True
#client = discord.Client(intents = default_intents)
from discord import FFmpegPCMAudio, PCMVolumeTransformer
import asyncio,os
from discord.ext import commands, tasks
from datetime import datetime, time, timedelta



bot = commands.Bot(command_prefix="§")
musics = {}
ytdl = youtube_dl.YoutubeDL()

@bot.event
async def on_ready():
    print("Le bot est prêt !")
    await bot.change_presence(activity=discord.Activity(name="§help ", type=1))
    channel = bot.get_channel(774765716280573972)
#    events = bot.get_guild(953446456310779964)
#    events = bot.fetch_guild(953446456310779964)
#    await channel.send(f'Cet évènement {events} me parait intéressant. Je pense y participer, pas vous ?')



class Video:
    def __init__(self, link):
        video = ytdl.extract_info(link, download=False)
        video_format = video["formats"][0]
        self.url = video["webpage_url"]
        self.stream_url = video_format["url"]


@bot.command()
async def leave(ctx):
    client = ctx.guild.voice_client
    await client.disconnect()
    musics[ctx.guild] = []

@bot.command()
async def resume(ctx):
    client = ctx.guild.voice_client
    if client.is_paused():
        client.resume()
        await ctx.send("*Resuming -* ▶️")

@bot.command()
async def pause(ctx):
    client = ctx.guild.voice_client
    if not client.is_paused():
        client.pause()
        await ctx.send("*Paused -* ⏸️")
"""
@bot.command()
async def show_queue(ctx):
    client = ctx.guild.voice_client
    #video = Video(url)
    #await ctx.send(f"Voici la queue: {video.url}")
    await ctx.send(f"queue songs , {musics} ")
    #print(f"{ctx.author.name}, {ctx.author.id} used command " + commandd + " used at ")"""


"""
@bot.command(name="current",description="Shows the current playing song.",usage="current",aliases=['np','nowplaying'])
async def current(self,ctx):
     player = self.bot.lavalink.player_manager.get(ctx.guild.id)
     embed=discord.Embed(title=player.current.title,url=f"https://youtube.com/watch?v={player.current.identifier}")
     await ctx.send(embed=embed)
"""


@bot.command()
async def skip(ctx):
    client = ctx.guild.voice_client
    client.stop()

def play_song(client, queue, song):
    source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio(song.stream_url , before_options="-reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5"))

    def next(_):
        if len(queue) > 0:
            new_song = queue[0]
            del queue[0]
            play_song(client, queue, new_song)
        else:
            asyncio.run_coroutine_threadsafe(client.disconnect(),bot.loop)

    client.play(source, after=next)
"""
@bot.command()
async def queue(ctx,url):
    client = ctx.guild.voice_client
    for i in musics[ctx.guild]:
        await ctx.send(f"musics : , {musics[i]} ")"""

@bot.command()
async def play(ctx, url):
    print("play")
    client = ctx.guild.voice_client
    #salon vocal du user de la cmd

    if client and client.channel:
        video = Video(url)
        musics[ctx.guild].append(video)
    else:
        channel = ctx.author.voice.channel
        video = Video(url)
        #on creer la queue
        musics[ctx.guild] = []
        #on se co au channel
        client = await channel.connect()
        await ctx.send (f"Je lance : {video.url}")
        play_song(client, musics[ctx.guild], video)



@bot.command(name="del")
async def delete(ctx, number: int):
    """del suivi du nombre de messages à supprimer"""
    messages = await ctx.channel.history(limit=number + 1).flatten()

    for each_message in messages:
        await each_message.delete()

@bot.command(name="grogu")
async def grogu(ctx):
    """pour une image sympatique"""
    await ctx.send(":slight_smile: ", file=discord.File('grogu_bot_name.JPEG'))

@bot.command(name="hello")
async def groguhello(ctx):
    """pour un GIF sympatique"""
    await ctx.send("https://tenor.com/view/grogu-star-wars-mandalorian-baby-yoda-gif-21008340")

@bot.command(name="KEKW")
async def KEKW(ctx):
    await ctx.send("https://tenor.com/view/el-risitas-risitas-wheezingrisitas-gif-21432588")

@bot.command(name="piece")
async def piece(ctx):
    """pour un pile ou face"""
    coin = ["pile", "face"]
    flipchoice = random.choice(coin)
    if str(flipchoice) == "pile":
        await ctx.send("C'est pile !")
    else:
        await ctx.send("C'est face !")
""""
@bot.command(name="dice")
async def dice(ctx):
    dice = list(range(1,20))
    flipchoice = random.choice(dice)
    await ctx.send(f"C'est {flipchoice} !")
"""
@bot.command(name="de")
async def de(ctx, number: int):
    """de suivi du nombre de face pour jeter un dé"""
    dice2 = list(range(1, number))
    flipchoice2 = random.choice(dice2)
    if (flipchoice2 == 1 or flipchoice2 == number):
        await ctx.send(f"C'est {flipchoice2}")
        await ctx.send("Crit! :game_die: ")
    else:
        await ctx.send(f"Vous lancez un dé {number}")
        await ctx.send(f"C'est {flipchoice2}")


@bot.command(name="dice")
async def de(ctx):
    """de suivi du nombre de face pour jeter un dé"""
    sumdices = 0
    dice = [4, 2, 1, 1, 3, 3]
    firstdice = random.choice(dice)
    seconddice = random.choice(dice)
    sumdices = firstdice + seconddice
    await ctx.send(f"Vous lancez deux dés: {firstdice},{seconddice}")
    await ctx.send(f"La somme fait {sumdices}")


@bot.command(name="sdice")
async def de(ctx, number: int):
    """suivi du nombre de dés efforts"""
    total = 0
    nbrsuccess = 0
    sdice = [1, 1, 2, 2, 3, "success"]
    for i in range (number):
        firstdice = random.choice(sdice)
        if firstdice == "success":
            nbrsuccess +=1
        else:
            total += firstdice
    await ctx.send(f"Nombre de succès {nbrsuccess}")
    await ctx.send(f"Reste des dés {total}")


@bot.command(name="ping")
async def ping(ctx):
    """pour une réponse pong"""
    await ctx.send("pong")

class AudioSourceTracked(discord.AudioSource):
    def __init__(self, source):
        self._source = source
        self.count_20ms = 0
    def read(self) -> bytes:
        data = self._source.read()
        if data:
            self.count_20ms += 1
        return data
    """
@bot.command()
async def progress(self,ctx) -> float:
    await ctx.send.self.count_20ms * 0.02 # count_20ms * 20ms
    
    """
#FFMPEG_OPTIONS = {'before_options': '-reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5', 'options': '-vn'}


@bot.command(name="matrixed")
async def matrixed(ctx):
    lematrixed = str(ctx.message.author.id == 241244446010769409)

    #if str(ctx.message.author) == "M.Rouge#7630":
    if str(ctx.message.author.id) == "241244446010769409":
        await ctx.send(f"coucou {ctx.message.author.mention}, vous êtes bien matrixed")
    else:
        await ctx.send(f"coucou {ctx.message.author.mention}, vous n'êtes pas matrixed")
        #await ctx.send(f"coucou {ctx.message.author.mention}, vous n'êtes pas matrixed comme {lematrixed}")
        await ctx.send("https://tenor.com/view/no-nooo-nope-eat-fingerwag-gif-23757070")


@bot.command(name="ei")
async def ei(ctx):
    if(ctx.author.voice):
        channel = ctx.message.author.voice.channel
        voice = await channel.connect()
        #source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio("song.https://www.youtube.com/watch?v=nGM375qYhN0l"))

        source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio("VO_JA_Raiden_Shogun_Elemental_Burst_03.mp3"))
        #source = FFmpegPCMAudio("VO_JA_Raiden_Shogun_Elemental_Burst_03.mp3")
        voice.play(source)
        await ctx.send("Inabikari, sunawachi eien nari.")
        await ctx.send("https://tenor.com/view/baal-genshin-impact-baal-baal-genshin-impact-childediluc-electro-archon-gif-22568311")
        while voice.is_playing():  # Checks if voice is playing
            await asyncio.sleep(1)  # While it's playing it sleeps for 1 second
        else:
            await asyncio.sleep(0)  # If it's not playing it waits 0 seconds
            while voice.is_playing():  # and checks once again if the bot is not playing
                break  # if it's playing it breaks
            else:
                await voice.disconnect()  # if not it disconnects
    else:
        await ctx.send("not in a voice channel, can't play")
        await ctx.send("Inabikari, sunawachi eien nari.")
        await ctx.send("https://tenor.com/view/baal-genshin-impact-baal-baal-genshin-impact-childediluc-electro-archon-gif-22568311")

@bot.command(name="qiqi")
async def qiqi(ctx):
    if(ctx.author.voice):
        channel = ctx.message.author.voice.channel
        voice = await channel.connect()
        #source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio("song.https://www.youtube.com/watch?v=nGM375qYhN0l"))

        source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio("qiqi.mp3"))
        #source = FFmpegPCMAudio("VO_JA_Raiden_Shogun_Elemental_Burst_03.mp3")
        voice.play(source)
        await ctx.send("Wakaranai.")
        await ctx.send("https://tenor.com/view/wakaranai-qiqi-genshin-genshin-impact-i-dont-know-gif-22722044")
        while voice.is_playing():  # Checks if voice is playing
            await asyncio.sleep(1)  # While it's playing it sleeps for 1 second
        else:
            await asyncio.sleep(0)  # If it's not playing it waits 0 seconds
            while voice.is_playing():  # and checks once again if the bot is not playing
                break  # if it's playing it breaks
            else:
                await voice.disconnect()  # if not it disconnects
    else:
        await ctx.send("not in a voice channel, can't play")
        await ctx.send("Wakaranai.")
        await ctx.send("https://tenor.com/view/wakaranai-qiqi-genshin-genshin-impact-i-dont-know-gif-22722044")


@bot.command(name="zenitsu")
async def zenitsu(ctx):
    if(ctx.author.voice):
        channel = ctx.message.author.voice.channel
        voice = await channel.connect()
        #source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio("song.https://www.youtube.com/watch?v=nGM375qYhN0l"))

        source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio("Zenitsu.mp3"))
        #source = FFmpegPCMAudio("VO_JA_Raiden_Shogun_Elemental_Burst_03.mp3")
        voice.play(source)
        await ctx.send("kaminari no kokyū ichi no kata hekireki issen rokuren.")
        await ctx.send("https://tenor.com/view/demon-slayer-kimetsu-no-yaiba-anime-zenitsu-agatsuma-getting-ready-gif-17922867")
        while voice.is_playing():  # Checks if voice is playing
            await asyncio.sleep(1)  # While it's playing it sleeps for 1 second
        else:
            await asyncio.sleep(0)  # If it's not playing it waits 0 seconds
            while voice.is_playing():  # and checks once again if the bot is not playing
                break  # if it's playing it breaks
            else:
                await voice.disconnect()  # if not it disconnects
    else:
        await ctx.send("not in a voice channel, can't play")
        await ctx.send("kaminari no kokyū ichi no kata hekireki issen rokuren.")
        await ctx.send("https://tenor.com/view/demon-slayer-kimetsu-no-yaiba-anime-zenitsu-agatsuma-getting-ready-gif-17922867")


@bot.command(name="zoro")
async def zoro(ctx):
    if(ctx.author.voice):
        channel = ctx.message.author.voice.channel
        voice = await channel.connect()
        #source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio("song.https://www.youtube.com/watch?v=nGM375qYhN0l"))

        source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio("zoro.mp3"))
        #source = FFmpegPCMAudio("VO_JA_Raiden_Shogun_Elemental_Burst_03.mp3")
        voice.play(source)
        await ctx.send("shishi sonson.")
        await ctx.send("https://tenor.com/view/shi-shi-son-son-gif-21039307")
        while voice.is_playing():  # Checks if voice is playing
            await asyncio.sleep(1)  # While it's playing it sleeps for 1 second
        else:
            await asyncio.sleep(0)  # If it's not playing it waits 0 seconds
            while voice.is_playing():  # and checks once again if the bot is not playing
                break  # if it's playing it breaks
            else:
                await voice.disconnect()  # if not it disconnects
    else:
        await ctx.send("not in a voice channel, can't play")
        await ctx.send("shishi sonson.")
        await ctx.send("https://tenor.com/view/shi-shi-son-son-gif-21039307")




@bot.command(name="paipo")
async def paipo(ctx):
    if(ctx.author.voice):
        channel = ctx.message.author.voice.channel
        voice = await channel.connect()
        source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio("fma_paipo.mp3"))
        #source = FFmpegPCMAudio("VO_JA_Raiden_Shogun_Elemental_Burst_03.mp3")
        voice.play(source)
        await ctx.send("Jugemu Jugemu Go-Kō-no-Surikire Kaijari-suigyo no Suigyō-matsu Unrai-matsu Fūrai-matsu Kū-Neru Tokoro ni Sumu Tokoro Yaburakōji no Burakōji Paipo Paipo Paipo no Shūringan Shūringan no Gūrindai Gūrindai no Ponpokopii no Ponpokonā no Chōkyūmei no Chōsuke.")
        await ctx.send("https://tenor.com/view/scar-fma-fmab-gif-9793202")
        while voice.is_playing():  # Checks if voice is playing
            await asyncio.sleep(1)  # While it's playing it sleeps for 1 second
        else:
            await asyncio.sleep(0)  # If it's not playing it waits 0 seconds
            while voice.is_playing():  # and checks once again if the bot is not playing
                break  # if it's playing it breaks
            else:
                await voice.disconnect()  # if not it disconnects
    else:
        await ctx.send("not in a voice channel, can't play")

@bot.command(name="issou")
async def issou(ctx):
    if(ctx.author.voice):
        channel = ctx.message.author.voice.channel
        voice = await channel.connect()
        source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio("issou.mp3"))
        #source = FFmpegPCMAudio("VO_JA_Raiden_Shogun_Elemental_Burst_03.mp3")
        voice.play(source)
        #await ctx.send("Jugemu Jugemu Go-Kō-no-Surikire Kaijari-suigyo no Suigyō-matsu Unrai-matsu Fūrai-matsu Kū-Neru Tokoro ni Sumu Tokoro Yaburakōji no Burakōji Paipo Paipo Paipo no Shūringan Shūringan no Gūrindai Gūrindai no Ponpokopii no Ponpokonā no Chōkyūmei no Chōsuke.")
        await ctx.send("https://tenor.com/view/el-risitas-risitas-wheezingrisitas-gif-21432588")
        while voice.is_playing():  # Checks if voice is playing
            await asyncio.sleep(1)  # While it's playing it sleeps for 1 second
        else:
            await asyncio.sleep(0)  # If it's not playing it waits 0 seconds
            while voice.is_playing():  # and checks once again if the bot is not playing
                break  # if it's playing it breaks
            else:
                await voice.disconnect()  # if not it disconnects
    else:
        await ctx.send("not in a voice channel, can't play")

@bot.command(name="escanor")
async def escanor(ctx):
    if(ctx.author.voice):
        channel = ctx.message.author.voice.channel
        voice = await channel.connect()
        source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio("escanor.mp3"))
        #source = FFmpegPCMAudio("VO_JA_Raiden_Shogun_Elemental_Burst_03.mp3")
        voice.play(source)
        await ctx.send("Tescanor.")
        await ctx.send("https://cdn.discordapp.com/attachments/241312674359541763/933077099969937458/static-assets-upload4939521969241265913.gif")
        while voice.is_playing():  # Checks if voice is playing
            await asyncio.sleep(1)  # While it's playing it sleeps for 1 second
        else:
            await asyncio.sleep(0)  # If it's not playing it waits 0 seconds
            while voice.is_playing():  # and checks once again if the bot is not playing
                break  # if it's playing it breaks
            else:
                await voice.disconnect()  # if not it disconnects
    else:
        await ctx.send("not in a voice channel, can't play")
        await ctx.send("Tescanor.")
        await ctx.send("https://cdn.discordapp.com/attachments/241312674359541763/933077099969937458/static-assets-upload4939521969241265913.gif")


@bot.command(name="heimer")
async def heimer(ctx):
    if(ctx.author.voice):
        channel = ctx.message.author.voice.channel
        voice = await channel.connect()
        source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio("https://static.wikia.nocookie.net/leagueoflegends/images/9/9d/Heimerdinger.attaque04.ogg/revision/latest?cb=20140308164544&path-prefix=fr"))
        #source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio("heimer.mp3"))
        voice.play(source)
        await ctx.send("Ramendinger.")
        await ctx.send("https://tenor.com/view/knock-knock-heimerdinger-arcane-knocking-let-me-out-gif-23704788")
        while voice.is_playing():  # Checks if voice is playing
            await asyncio.sleep(1)  # While it's playing it sleeps for 1 second
        else:
            await asyncio.sleep(0)  # If it's not playing it waits 0 seconds
            while voice.is_playing():  # and checks once again if the bot is not playing
                break  # if it's playing it breaks
            else:
                await voice.disconnect()  # if not it disconnects
    else:
        await ctx.send("not in a voice channel, can't play")
        await ctx.send("Ramendinger.")
        await ctx.send("https://tenor.com/view/knock-knock-heimerdinger-arcane-knocking-let-me-out-gif-23704788")


@bot.command(name="megumin")
async def megumin(ctx):
    if(ctx.author.voice):
        channel = ctx.message.author.voice.channel
        voice = await channel.connect()
        source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio("megumin.mp3"))
        #source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio("heimer.mp3"))
        voice.play(source)
        await ctx.send("Waganawa Megumin.")
        await ctx.send("https://tenor.com/view/waga-namaewa-megumi-gif-24177131")
        while voice.is_playing():  # Checks if voice is playing
            await asyncio.sleep(1)  # While it's playing it sleeps for 1 second
        else:
            await asyncio.sleep(0)  # If it's not playing it waits 0 seconds
            while voice.is_playing():  # and checks once again if the bot is not playing
                break  # if it's playing it breaks
            else:
                await voice.disconnect()  # if not it disconnects
    else:
        await ctx.send("not in a voice channel, can't play")
        await ctx.send("Waganawa Megumin.")
        await ctx.send("https://tenor.com/view/waga-namaewa-megumi-gif-24177131")


@bot.command(name="nezuko")
async def nezuko(ctx):
    if(ctx.author.voice):
        channel = ctx.message.author.voice.channel
        voice = await channel.connect()
        source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio("nezuko.mp3"))
        #source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio("heimer.mp3"))
        voice.play(source)
        await ctx.send("Nezuko chan.")
        await ctx.send("https://tenor.com/view/zenitsu-nezuko-chan-happy-anime-gif-14810985")
        while voice.is_playing():  # Checks if voice is playing
            await asyncio.sleep(1)  # While it's playing it sleeps for 1 second
        else:
            await asyncio.sleep(0)  # If it's not playing it waits 0 seconds
            while voice.is_playing():  # and checks once again if the bot is not playing
                break  # if it's playing it breaks
            else:
                await voice.disconnect()  # if not it disconnects
    else:
        await ctx.send("not in a voice channel, can't play")
        await ctx.send("Nezuko chan.")
        await ctx.send("https://tenor.com/view/zenitsu-nezuko-chan-happy-anime-gif-14810985")



@bot.command(name="nisqy")
async def nisqy(ctx):
    if(ctx.author.voice):
        channel = ctx.message.author.voice.channel
        voice = await channel.connect()
        source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio("nisqy.mp3"))
        #source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio("heimer.mp3"))
        voice.play(source)
        #await ctx.send("Nezuko chan.")
        #await ctx.send("https://tenor.com/view/zenitsu-nezuko-chan-happy-anime-gif-14810985")
        while voice.is_playing():  # Checks if voice is playing
            await asyncio.sleep(1)  # While it's playing it sleeps for 1 second
        else:
            await asyncio.sleep(0)  # If it's not playing it waits 0 seconds
            while voice.is_playing():  # and checks once again if the bot is not playing
                break  # if it's playing it breaks
            else:
                await voice.disconnect()  # if not it disconnects
    else:
        await ctx.send("not in a voice channel, can't play")
        #await ctx.send("Nezuko chan.")
        #await ctx.send("https://tenor.com/view/zenitsu-nezuko-chan-happy-anime-gif-14810985")


@bot.command(name="chips")
async def chips(ctx):
    if(ctx.author.voice):
        channel = ctx.message.author.voice.channel
        voice = await channel.connect()
        source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio("chips.mp3"))
        await ctx.send("https://j.gifs.com/gp8lWG.gif")
        voice.play(source)
        while voice.is_playing():  # Checks if voice is playing
            await asyncio.sleep(1)  # While it's playing it sleeps for 1 second
        else:
            await asyncio.sleep(0)  # If it's not playing it waits 0 seconds
            while voice.is_playing():  # and checks once again if the bot is not playing
                break  # if it's playing it breaks
            else:
                await voice.disconnect()  # if not it disconnects
    else:
        await ctx.send("not in a voice channel, can't play")
        await ctx.send("https://j.gifs.com/gp8lWG.gif")

"""
@bot.command(name="gwent")
async def gwent(ctx):
    if (ctx.author.voice):
        channel = ctx.message.author.voice.channel
        voice = await channel.connect()
        await ctx.send("§play https://www.youtube.com/watch?v=8WeE9AisvMY")

"""
""""
@tasks.loop(seconds=2)
async def change_status():
    channel = bot.get_channel(774765716280573972)
    await channel.send('here')
    await bot.change_presence(activity=discord.Game('online'))
    print('test')
"""


@bot.command(name="chirac")
async def chirac(ctx):
    if(ctx.author.voice):
        channel = ctx.message.author.voice.channel
        voice = await channel.connect()
        source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio("chirac.mp3"))
        #source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio("heimer.mp3"))
        voice.play(source)
        #await ctx.send("Nezuko chan.")
        #await ctx.send("https://tenor.com/view/zenitsu-nezuko-chan-happy-anime-gif-14810985")
        while voice.is_playing():  # Checks if voice is playing
            await asyncio.sleep(1)  # While it's playing it sleeps for 1 second
        else:
            await asyncio.sleep(0)  # If it's not playing it waits 0 seconds
            while voice.is_playing():  # and checks once again if the bot is not playing
                break  # if it's playing it breaks
            else:
                await voice.disconnect()  # if not it disconnects
    else:
        await ctx.send("not in a voice channel, can't play")
        #await ctx.send("Nezuko chan.")
        #await ctx.send("https://tenor.com/view/zenitsu-nezuko-chan-happy-anime-gif-14810985")


@bot.command(name="disney")
async def piece(ctx):
    """pour un disney"""
    text = open("disneyList.txt","r")
    disney = text.readlines()
    choice = random.choice(disney)
    await ctx.send(f"Le disney du jour est...... {choice}")        


WHEN = time (9, 0,0) #date
channel_id = 241244756238270465

async def called_once_a_day():  # Fired every day
    await bot.wait_until_ready()  # Make sure your guild cache is ready so the channel can be found via get_channel
    channel = bot.get_channel(channel_id) # Note: It's more efficient to do bot.get_guild(guild_id).get_channel(channel_id) as there's less looping involved, but just get_channel still works fine
    await channel.send("Quest !")
async def background_task():
    now = datetime.now()
    if now.time() > WHEN:  # Make sure loop doesn't start after {WHEN} as then it will send immediately the first time as negative seconds will make the sleep yield instantly
        tomorrow = datetime.combine(now.date() + timedelta(days=1), time(0))
        seconds = (tomorrow - now).total_seconds()  # Seconds until tomorrow (midnight)
        await asyncio.sleep(seconds)   # Sleep until tomorrow and then the loop will start
    while True:
        now = datetime.now() # You can do now() or a specific timezone if that matters, but I'll leave it with utcnow
        target_time = datetime.combine(now.date(), WHEN)  # 6:00 PM today (In UTC)
        seconds_until_target = (target_time - now).total_seconds()
        await asyncio.sleep(seconds_until_target)  # Sleep until we hit the target time
        await called_once_a_day()  # Call the helper function that sends the message
        tomorrow = datetime.combine(now.date() + timedelta(days=1), time(0))
        seconds = (tomorrow - now).total_seconds()  # Seconds until tomorrow (midnight)
        await asyncio.sleep(seconds)   # Sleep until tomorrow and then the loop will start a new iteration


"""
@tasks.loop(seconds=0.1 , count=10000)
async def slow_count():
    print(slow_count.current_loop)
    channel_id = 427112773927829514
    await bot.wait_until_ready()  # Make sure your guild cache is ready so the channel can be found via get_channel
    channel = bot.get_channel(channel_id) # Note: It's more efficient to do bot.get_guild(guild_id).get_channel(channel_id) as there's less looping involved, but just get_channel still works fine
    await channel.send("C'est honteux je veux jouer moi aussi")
    await channel.send("https://tenor.com/view/cest-honteux-honteux-mistermv-potatoz-gif-14793622")

@slow_count.after_loop
async def after_slow_count():
    print('done!')

slow_count.start()"""
bot.loop.create_task(background_task())
bot.run(os.getenv("TOKEN"))
#$del 2
